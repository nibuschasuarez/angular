import { Injectable } from '@angular/core';
import {  AbstractControl, FormControl, ValidationErrors  } from '@angular/forms';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  constructor() { }
  noZaralai(control: FormControl): {[s: string]: boolean }{
    
    console.log(control.value);
    if(control.value?.toLowerCase() === 'zaralai'){
      return{
        noZaralai: true
      }
    }
    
    return null as any;
  }
  passwordsIguales(pass1Name: string, pass2Name: string): ValidationErrors | null {
    return (controls:AbstractControl) => {
      const pass1Control = controls.get(pass1Name)?.value;
      const pass2Control = controls.get(pass2Name)?.value;

      if(pass1Control === pass2Control){
        return controls.get(pass2Name)?.setErrors(null);
      }else{
        return controls.get(pass2Name)?.setErrors({
          noEsIgual: true});
        }
    };

  }

  existeUsuario(control: FormControl):Promise<any> |  Observable<any> {
    return new Promise((resolve, reject) => {
      console.log('hola');
      
      setTimeout(() => {
        if(control.value === 'jerjes'){
          resolve({ existe: true});
        } else {
          resolve(null);
        }
      }, 3500);
    });
  }
}

 